module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      rules: {
        '@typescript-eslint/no-shadow': ['error'],
        'no-shadow': 'off',
        'no-undef': 'off',
      },
    },
  ],

  rules: {
    'no-multiple-empty-lines': ['error', { max: 2 }],
    'no-return-await': 'error',
    'no-throw-literal': 'error',
    'no-await-in-loop': 'error',
    'no-duplicate-imports': 'error',
    'id-length': ['error', { min: 2, properties: 'never', exceptions: ['R'] }],
    eqeqeq: ['error', 'smart'],
    'max-depth': ['error', 4],
    'react/no-deprecated': 'error',
    'no-console': 'error',
    curly: 'off',
  },
};
